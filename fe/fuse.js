const { FuseBox, Babel7Plugin, WebIndexPlugin } = require("fuse-box");
const {task} = require("fuse-box/sparky")
const fs = require("fs");

const defaultPlugins = ({bundles} = {}) => [Babel7Plugin({
  sourceMaps: true,
  presets: ["@babel/preset-env", "@babel/preset-react"],
  plugins: ["@babel/plugin-proposal-class-properties"]
}), WebIndexPlugin({
  bundles,
  templateString: "<html><body><div id='app'/>$bundles</body></html>"
})]

const init = (options = {}) => {
  return FuseBox.init(fuseOptions(options))
}

const fuseOptions = (options) => {
  const customizedPlugins = defaultPlugins(options);
  return Object.assign({}, {
    homeDir: "app",
    hash: true,
    target: "browser@es6",
    output: "dist/$name.$hash.js",
    allowSyntheticDefaultImports: true
  }, options, {plugins: customizedPlugins})
}

function writeMeta(name) {
  return (proc) => {
    const filename = proc.bundle.context.output.lastPrimaryOutput.filename
    fs.writeFileSync(`./dist/${name}.meta.json`, JSON.stringify({name: filename}))
  }
}

const vendor = (fuse) => {
  fuse.bundle("vendor")
    .instructions("~ main/index.js")
    .completed(writeMeta("vendor"))
}

const main = (fuse) => {
  fuse.bundle("main")
    .instructions("> [main/index.js]")
}

const todo = ({dev} = {}) => (fuse) => {
  fuse.bundle("todo")
    .watch()
    .instructions(dev ? "> [todo/index.js]" : "![todo/todo.js]")
    .completed(writeMeta("todo"))
}

const calendar = ({dev} = {}) => (fuse) => {
  fuse.bundle("calendar")
    .instructions(dev ? "> [calendar/index.js]" : "![calendar/calendar.js]")
    .completed(writeMeta("calendar"))
}

const dev = (fuse) => fuse.dev()
const run = (fuse) => fuse.run()

const apply = (fuse, decorators) => decorators.forEach(d => d(fuse))

task("todo", () => {
  const fuse = init();
  apply(fuse, [todo(), run]);
})

task("todo:dev", () => {
  const fuse = init();

  apply(fuse, [vendor, todo({dev: true}), dev, run]);
})

task("calendar", () => {
  const fuse = init();
  apply(fuse, [calendar(), run]);
})

task("calendar:dev", () => {
  const fuse = init();

  apply(fuse, [vendor, todo({dev: true}), dev, run]);
})

task("micros", [
  "&todo",
  "&calendar"
])

task("main:dev", () => {
  const fuse = init({
    bundles: ["vendor", "main"]
  });

  apply(fuse, [vendor, main, dev, run]);
})