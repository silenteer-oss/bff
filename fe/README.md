# Structure

### Backend
Containing backend code for storing todo in database
- ws.js, a simple implementation using fastifyjs to host a websocket server
- event handlers are in todo.js. There are `broadcast` to send message to all clients and `send` to reply to whoever triggered the message
- event folder contains mostly strings

### Frontend
Containing a simple front-end logic to send todo to the backend via websocket
- ws.js is a simple websocket implementation
- Each microfrontend should have a store of its own, the shape will be determined by the corresponding event from backend
- Example can be found in todo_store. Store implements following things
1. send signal to backend via websocket
2. receive signal from backend via websocket
3. passing data to the component

### Events
Code shared between backend and frontend, mostly for "autocomplete" purposes

# How to run

### Start Backend app
- npm i
- node .

### Start Frontend app
- npm i
- node fuse.js todo:dev

The webdev will then be listening on port `4444`