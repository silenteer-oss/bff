var conn = new WebSocket("ws://localhost:3000")
var pendingQueue = []

conn.onclose = (evt) => {
    console.log("Connection", "closed")
};

conn.onopen = (evt) => {
    console.log("Connection", "openned");
    pendingQueue.forEach(({address, body}) => doSend(address, body))
    pendingQueue = []
};

conn.onmessage = (evt) => {
    const {address, body} = JSON.parse(evt.data);
    registered[address] && registered[address](body)
    console.log(body)
};

function doSend(address, object) {
    const timestamp = new Date().getUTCMilliseconds();
    const msg = JSON.stringify({
        "address": address,
        "replyTo": timestamp,
        "body": object
    });
    conn.send(msg);
}

function send(address, object) {
    
    if (conn.readyState === conn.OPEN) {
        doSend(address, object)  
    } else {
        console.log("It's not ready yet, adding to queue")
        pendingQueue.push({address, object})
    }
}

const registered = {};

function subscribe(subject, handler) {
    registered[subject] = handler;
}

function unsubscribe() {
    arguments.forEach(arg => delete registered[arg])
}

export default {send, subscribe, unsubscribe}