import {useEffect, useRef, useState} from "react";
import ws from "./ws";

const combiner = ({initialState, store}, Component) => {
    const [state, setState] = useState({initialState});

    const subs = (topic, handler) => {
        ws.subscribe(topic, (data) => {
            setState(handler(data, state));
        });
    };

    // useEffect to trigger on the first time, [] deps to make sure it's not gonna be re-initialized
    const storeRef = useRef(null);
    useEffect(() => {
        storeRef.current = store({subs, send});
    }, []);

    // useEffect to clean up on componentWillUnmount
    useEffect(() => () => {
        ws.unsubscribe();
    }, []);

    return <Component store={storeRef.current} {...state}/>
};

export default combiner;