import {useEffect, useState} from 'react'
import Todo from "../events/todo"

const initialState = {};

const todoStore = ({subs, send}) => {
    subs(Todo.TODO_ADDED, (todo, {todos}) => {
        return { todos: [...todos, todo] }
    });

    subs(Todo.TODO_REMOVED, (todo, {todos}) => {
        return { todos: todos.filter(t => t !== todo) };
    });

    return {
        doGet: () => send(Todo.GET_TODOS, todo),
        prepareAdd: () => {

        }
    }
};

function todoStore ({subs, send}) {
    const [state, setState] = useState({todos: []});

    subs(Todo.TODO_ADDED, (todo, {todos}) => {
        return { todos: [...todos, todo] }
    });

    subs(Todo.TODO_REMOVED, (todo, {todos}) => {
        return { todos: todos.filter(t => t !== todo) };
    });
}

export default {initialState, store: todoStore};