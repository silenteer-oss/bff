import React, { useState, useEffect } from "react";

function TodoInput({ onSubmit }) {
    const [v, setInput] = useState("")
    return <div>
        <input value={v} onChange={(e) => setInput(e.target.value)} type="text"/>
        <button onClick={() => onSubmit(v)}>add</button>
    </div>
}

function TodoDisplay({ text, onRemove }) {
    return <div>
        {text}
        <br />
        <button onClick={onRemove.bind(this, text)}>Remove</button>
    </div>
}

function Todos(props) {
    const { state, doGet, prepareAdd, doRemove, doAdd } = props;
    const { todos, isAddingTodo } = todoStore;
    useEffect(doGet, []);

    return <div>
        {todos.length === 0 && <div>Loading...</div>}
        {todos.map((todo, idx) => <TodoDisplay key={idx} onRemove={doRemove} text={todo} />)}
        <button onClick={prepareAdd}>Add todo</button>
        <br />
        {isAddingTodo && <TodoInput onSubmit={doAdd} />}
    </div>
}

export default Todos;