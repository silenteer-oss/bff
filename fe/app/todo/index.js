import React from "react";
import ReactDOM from "react-dom";

import TodoView from "./todo";
import TodoStore from "./todo_store";
import combine from "../combiner";

ReactDOM.render(combine(TodoStore, TodoView), document.getElementById("app"))