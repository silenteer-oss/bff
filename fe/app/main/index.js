import React from "react";
import ReactDOM from "react-dom";
import { observable, decorate, action } from "mobx";
import "mobx-react";

import(window.location.href + "todo.meta.json")
    .then(meta => import(window.location.href + meta.name))
    .then(_ => {
        const Todos = FuseBox.import("default/todo/todo").default
        ReactDOM.render(<Todos />, document.getElementById("app"))
    })

