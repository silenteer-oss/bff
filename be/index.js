const server = require("./ws");
const todoService = require("./todo");

server.register(todoService);
server.start();