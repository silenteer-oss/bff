const todos = require("../events/todo")

var dataStore = [];

module.exports = {
    [todos.GET_TODOS]: async ({send}) => {
        send(todos.TODOS_LOADED, dataStore);
    },
    [todos.ADD_TODO]: async ({broadcast}, todo) => {
        dataStore.push(todo)
        broadcast(todos.TODO_ADDED, todo);
    },
    [todos.REMOVE_TODO]: async ({broadcast}, todo) => {
        dataStore = dataStore.filter(t => t !== todo);
        broadcast(todos.TODO_REMOVED, todo);
    }
}