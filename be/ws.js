const fastify = require('fastify')({
    logger: true
})

const services = [];

function send(connection) {
    return (address, object) => {
        connection.socket.send(JSON.stringify({
            address: address,
            body: object
        }))
    }
}

var clients = []

function broadcast(address, message) {
    console.log("Broadcasting to", address, "out of", clients.length, message)
    clients.forEach(client => send(client)(address, message))
}

fastify.register(require('fastify-websocket'))

fastify.get('/', { websocket: true }, async (connection, req) => {
    clients.push(connection);

    connection.socket.on('message', async message => {
        const { address, body, timestamp } = JSON.parse(message);

        const handler = services.find(service => service[address]);
        handler && await handler[address]({ send: send(connection), broadcast }, body);
    })

    connection.socket.on('close', () => {
        clients = clients.filter(c => c !== connection)
    })
})

module.exports = {
    start: () => {
        fastify.listen(3000, (err, address) => {
            if (err) throw err
            fastify.log.info(`server listening on ${address}`)
        })
    },
    register: (service) => {
        services.push(service)
    }
}