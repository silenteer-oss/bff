const ADD_TODO = "todo.add"
const REMOVE_TODO = "todo.remove"
const GET_TODOS = "todo.getList"

const ACTIONS = [ADD_TODO, REMOVE_TODO, GET_TODOS];

const TODOS_LOADED = "todo.loaded";
const TODO_ADDED = "todo.added";
const TODO_REMOVED = "todo.removed";

const EVENTS = [TODOS_LOADED, TODO_ADDED, TODO_REMOVED]

module.exports = {
    ADD_TODO, REMOVE_TODO, GET_TODOS,
    ACTIONS,

    TODOS_LOADED,
    TODO_ADDED,
    TODO_REMOVED,
    EVENTS

}