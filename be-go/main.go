package main

import (
	"encoding/json"
	"flag"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"container/list"
)

var addr = flag.String("addr", "localhost:8080", "http service address")
var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type content struct {
	Address string `json:"method"`
	Body	interface{} `json:"body"`
}

type tool struct {
	content
	send func(interface{}, interface{})
	broadcast func(interface{}, interface{})
}

type TodoHandler struct {}

var store = list.New()

func (t *TodoHandler) Handle(sender *tool, c content) {
	switch c.Address {
	case "todo.add":
		store.PushBack(c.Body)
		sender.send("todo.added", c.Body)
	case "todo.remove":
		for e := store.Front(); e != nil; e.Next() {
			if e.Value == c.Body {
				store.Remove(e)
				break
			}
		}
		sender.send("todo.removed", c.Body)
	}
}

type Handler interface {
	Handle(* interface{}) interface{}
}

func ws(w http.ResponseWriter, r *http.Request) {
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}
	defer c.Close()
	for {
		_, message, err := c.ReadMessage()

		if err != nil {
			log.Println("read:", err)
			break
		}
		var c = &content{}
		err = json.Unmarshal(message, c)
		if err != nil {
			log.Println("Message is not in JSON format")
		}


	}
}

func main() {
	flag.Parse()
	log.SetFlags(0)
	http.HandleFunc("/", ws)
	log.Fatal(http.ListenAndServe(*addr, nil))
}